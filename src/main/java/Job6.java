import comparator.IntWritableInvertedComparator;
import node.NodePrefix;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.ToolRunner;
import org.semanticweb.yars.nx.Node;
import org.semanticweb.yars.nx.parser.NxParser;
import reducer.BasicSumReducer;
import reducer.Top10Reducer;
import writable.BasicWritable;

import java.io.IOException;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by andre on 27/04/2017.
 */
public class Job6 {

    private final static Class thisClass = Job6.class;

    static int printUsage() {
        System.out.println(thisClass.getName() + " [-m <maps>] [-r <reduces>] <input> <output>");
        ToolRunner.printGenericCommandUsage(System.out);
        return -1;
    }

    public static void main(String[] args) throws Exception {

        /**
         * Job 1
         */

        Configuration conf = new Configuration();
        JobProperties prop = new JobProperties(thisClass);

        conf.setInt("mapreduce.job.maps", prop.getMaps());
        conf.setInt("mapreduce.job.reduces", prop.getReduces());

        Path input = new Path(prop.getInputPath());
        Path output = new Path(prop.getOutputPath() + "/out");

        Path output_int = new Path(prop.getOutputPath() + "/int");

        Job job = Job.getInstance(conf);
        job.setJarByClass(thisClass);
        job.setJobName(thisClass.getName());

        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output_int);

        job.setMapperClass(JobMapper.class);
        job.setReducerClass(JobReducer.class);

        job.setSortComparatorClass(Text.Comparator.class);
        job.setGroupingComparatorClass(Text.Comparator.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.waitForCompletion(true);

        /**
         * Job 2
         */

        conf.setInt("mapreduce.job.reduces", 1);

        Job job2 = Job.getInstance(conf);
        job2.setJarByClass(thisClass);
        job2.setJobName(thisClass.getName());

        FileInputFormat.addInputPath(job2, output_int);
        FileOutputFormat.setOutputPath(job2, output);

        job2.setMapperClass(Job2Mapper.class);
        job2.setCombinerClass(Top10Reducer.class);
        job2.setReducerClass(Top10Reducer.class);

        job2.setSortComparatorClass(IntWritableInvertedComparator.class);
        job2.setGroupingComparatorClass(IntWritableInvertedComparator.class);

        job2.setInputFormatClass(SequenceFileInputFormat.class);
        job2.setOutputFormatClass(TextOutputFormat.class);

        job2.setOutputKeyClass(IntWritable.class);
        job2.setOutputValueClass(Text.class);

        job2.waitForCompletion(true);


    }

    public static class JobMapper extends Mapper<LongWritable, Text, Text, Text> {


        private Text keyLine = new Text();
        private Text valueLine = new Text();

        @Override
        protected void cleanup(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.cleanup(context);
        }


        private String nodeToString(Node[] n) {

            return n[0].toString() + " " +
                    n[1].toString() + " " +
                    n[2].toString() + " .";
        }

        private String nodeContextToString(Node[] n) {

            if (n.length < 4) {
                return "emptyContext";
            }

            return n[3].toString();
        }

        @Override
        protected void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            Scanner scanner = new Scanner(value.toString());
            scanner.useDelimiter("\n");

            NxParser nxp = new NxParser();
            nxp.parse(scanner);

            for (Node[] nx : nxp) {

                keyLine.set(nodeToString(nx));
                valueLine.set(nodeContextToString(nx));

                context.write(keyLine, valueLine);
            }
        }


        @Override
        public void run(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.run(context);
        }

        @Override
        protected void setup(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.setup(context);
        }

    }


    public static class JobReducer extends Reducer<Text, Text, Text, IntWritable> {

        private IntWritable valueLine = new IntWritable();

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            super.setup(context);
        }

        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
//            super.reduce(key, values, context);
            SortedSet<String> valueSet = new TreeSet<>();

            for (Text value : values) {
                valueSet.add(value.toString());
            }

            valueLine.set(valueSet.size());
            context.write(key, valueLine);
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            super.cleanup(context);
        }

        @Override
        public void run(Context context) throws IOException, InterruptedException {
            super.run(context);
        }
    }

    public static class Job2Mapper extends Mapper<Text, IntWritable, IntWritable, Text> {

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            super.cleanup(context);
        }


        @Override
        protected void map(Text key, IntWritable value, Context context) throws IOException, InterruptedException {

            context.write(value, key);
        }

        @Override
        public void run(Context context) throws IOException,
                InterruptedException {
            super.run(context);
        }

        @Override
        protected void setup(Context context) throws IOException,
                InterruptedException {
            super.setup(context);
        }

    }

}
