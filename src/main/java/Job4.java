import comparator.IntWritableInvertedComparator;
import node.NodePrefix;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.ToolRunner;
import reducer.Top10Reducer;

import java.io.IOException;

public class Job4 {

    private final static Class thisClass = Job4.class;

    static int printUsage() {
        System.out.println(thisClass.getName() + " [-m <maps>] [-r <reduces>] <input> <output>");
        ToolRunner.printGenericCommandUsage(System.out);
        return -1;
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();

        JobProperties prop = new JobProperties(thisClass);

        conf.setInt("mapreduce.job.maps", prop.getMaps());
        conf.setInt("mapreduce.job.reduces", prop.getReduces());

        Path input = new Path(prop.getInputPath());
        Path output = new Path(prop.getOutputPath());

        Job job = Job.getInstance(conf);
        job.setJarByClass(thisClass);
        job.setJobName(thisClass.getName());

        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);

        job.setMapperClass(JobMapper.class);
        job.setCombinerClass(Top10Reducer.class);
        job.setReducerClass(Top10Reducer.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setSortComparatorClass(IntWritableInvertedComparator.class);
        job.setGroupingComparatorClass(IntWritableInvertedComparator.class);

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);

        job.waitForCompletion(true);
    }


    public static class JobMapper extends Mapper<Text, IntWritable, IntWritable, Text> {

        private Text line = new Text();

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            super.cleanup(context);
        }

        @Override
        protected void map(Text key, IntWritable value, Context context)
                throws IOException, InterruptedException {

            String keyString = key.toString();
            if (keyString.startsWith(NodePrefix.out)) {
                line.set(keyString.substring(3));
                context.write(value, line);
            }

        }

        @Override
        public void run(Context context) throws IOException,
                InterruptedException {
            super.run(context);
        }

        @Override
        protected void setup(Context context) throws IOException,
                InterruptedException {
            super.setup(context);
        }

    }



}
