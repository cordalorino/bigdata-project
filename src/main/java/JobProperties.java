import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Properties;

/**
 * Created by andre on 30/04/2017.
 */
public class JobProperties {

    private Properties prop = new Properties();

    private String inputPath, outputPath;
    private int maps, reduces;

    public JobProperties(Class jobClass) throws NoSuchFieldException {

        InputStream input = null;
        String filename = "properties/" + jobClass.getName();

        try {
            input = new FileInputStream(filename);

            prop.load(input);

            this.inputPath = prop.getProperty("inputPath");
            this.outputPath = prop.getProperty("outputPath");
            this.maps = new Integer(prop.getProperty("maps"));
            this.reduces = new Integer(prop.getProperty("reduces"));

        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println(filename + " file not found!");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        for (Field field : this.getClass().getFields()) {
            if (field.equals(null)) {
                throw new NoSuchFieldException(field.getName());
            }
        }


    }

    public String getString(String key) {
        return prop.getProperty(key);
    }

    public String getInputPath() {
        return inputPath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public int getMaps() {
        return maps;
    }

    public int getReduces() {
        return reduces;
    }
}
