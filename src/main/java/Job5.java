import node.NodePrefix;
import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.ToolRunner;
import reducer.BasicSumReducer;
import writable.BasicWritable;

import javax.xml.soap.Node;
import java.io.IOException;

/**
 * Created by andre on 27/04/2017.
 */
public class Job5 {

    private final static Class thisClass = Job5.class;

    static int printUsage() {
        System.out.println(thisClass.getName() + " [-m <maps>] [-r <reduces>] <input> <output>");
        ToolRunner.printGenericCommandUsage(System.out);
        return -1;
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();

        JobProperties prop = new JobProperties(thisClass);

        conf.setInt("mapreduce.job.maps", prop.getMaps());
        conf.setInt("mapreduce.job.reduces", prop.getReduces());

        Path input = new Path(prop.getInputPath());
        Path output = new Path(prop.getOutputPath());

        Job job = Job.getInstance(conf);
        job.setJarByClass(thisClass);
        job.setJobName(thisClass.getName());

        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);

        job.setMapperClass(JobMapper.class);
        job.setReducerClass(JobReducer.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FloatWritable.class);

        job.waitForCompletion(true);
    }

    public static class JobMapper extends Mapper<Text, IntWritable, Text, IntWritable> {

        @Override
        protected void cleanup(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.cleanup(context);
        }


        private String[] prefixToEmit = {
                NodePrefix.tri, NodePrefix.eco,
                NodePrefix.bns, NodePrefix.bno};

        @Override
        protected void map(Text key, IntWritable value, Context context)
                throws IOException, InterruptedException {

            String keyString = key.toString();
            String keyPrefix = keyString.substring(0, 3);

            if (ArrayUtils.contains(prefixToEmit, keyPrefix)) {
                context.write(key, value);
            }

        }

        @Override
        public void run(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.run(context);
        }

        @Override
        protected void setup(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.setup(context);
        }

    }


    public static class JobReducer extends Reducer<Text, IntWritable, Text, FloatWritable> {

        private int triple = -1;
        private int emptyContext = 0;
        private int blankNodeSubject = 0;
        private int blankNodeObject = 0;

        private Text line = new Text();
        private FloatWritable valueOutput = new FloatWritable();

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {

            if (triple > 0) {
                line.set("triple");
                valueOutput.set(triple);
                context.write(line, valueOutput);
            } else {
                return;
            }


            /**
             * write emptyContext
             */
            line.set("emptyContext");
            valueOutput.set(emptyContext);
            context.write(line, valueOutput);
            line.set("emptyContextPercentage");
            valueOutput.set((float) emptyContext / triple);
            context.write(line, valueOutput);


            /**
             * write blankNodeSubject
             */
            line.set("blankNodeSubject");
            valueOutput.set(blankNodeSubject);
            context.write(line, valueOutput);
            line.set("blankNodeSubjectPercentage");
            valueOutput.set((float) blankNodeSubject / triple);
            context.write(line, valueOutput);


            /**
             * write blankNodeObject
             */
            line.set("blankNodeObject");
            valueOutput.set(blankNodeObject);
            context.write(line, valueOutput);
            line.set("blankNodeObjectPercentage");
            valueOutput.set((float) blankNodeObject / triple);
            context.write(line, valueOutput);


            super.cleanup(context);

        }

        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context)
                throws IOException, InterruptedException {

//            context.write(key, values.iterator().next());

            String keyString = key.toString();
            String keyPrefix = keyString.substring(0, 3);

            int value = values.iterator().next().get();

            switch (keyPrefix) {
                case NodePrefix.tri:
                    triple = value;
                    break;
                case NodePrefix.eco:
                    emptyContext = value;
                    break;
                case NodePrefix.bns:
                    blankNodeSubject = value;
                    break;
                case NodePrefix.bno:
                    blankNodeSubject = value;
                    break;
            }


        }


        @Override
        public void run(Context context) throws IOException, InterruptedException {
            super.run(context);
        }

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            super.setup(context);
        }
    }

}
