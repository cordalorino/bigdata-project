package reducer;

import node.NodePrefix;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;

/**
 * Created by andre on 27/04/2017.
 */

public class MultipleOutputReducer2_3 extends Reducer<WritableComparable, IntWritable, WritableComparable, IntWritable> {
    public static final String N_OUT_DEGREE = "outDegree";
    public static final String N_IN_DEGREE = "inDegree";
    @Override
    protected void cleanup(Context context) throws IOException,
            InterruptedException {
        // TODO Auto-generated method stub
        super.cleanup(context);
    }

    private MultipleOutputs mos;
    private IntWritable line = new IntWritable();
    private IntWritable sumWritable = new IntWritable();

    @Override
    protected void reduce(WritableComparable key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {

        int sum = 0;

        for (IntWritable value : values) {
            sum += value.get();
        }

        String keyString = key.toString();
        sumWritable.set(sum);
        line.set(Integer.parseInt(keyString.substring(3)));
        if (keyString.startsWith(NodePrefix.inp)) {
            mos.write(N_IN_DEGREE, line , sumWritable);
        } else if (keyString.startsWith(NodePrefix.out)) {
            mos.write(N_OUT_DEGREE, line , sumWritable);
        }


    }

    @Override
    public void run(Context arg0) throws IOException, InterruptedException {
        // TODO Auto-generated method stub
        super.run(arg0);
    }

    @Override
    protected void setup(Context context) throws IOException,
            InterruptedException {
        // TODO Auto-generated method stub
        super.setup(context);
        mos = new MultipleOutputs(context);
    }
}