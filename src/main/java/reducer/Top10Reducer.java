package reducer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class Top10Reducer extends Reducer<IntWritable, Text, IntWritable, Text> {

    int outputsLeft = 10;

    @Override
    protected void cleanup(Context context) throws IOException,
            InterruptedException {
        super.cleanup(context);
    }

    @Override
    protected void reduce(IntWritable key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {

        for (Text value : values) {
            if (outputsLeft != 0) {
                context.write(key, value);
                outputsLeft--;
            }else{
                return;
            }
        }
    }

    /**
     * Check if the number of outputs left is bigger than 0 before running the reduce function
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void run(Context context) throws IOException, InterruptedException {
        this.setup(context);

        try {
            while(context.nextKey() && this.outputsLeft > 0) {
                this.reduce(context.getCurrentKey(), context.getValues(), context);
            }
        } finally {
            this.cleanup(context);
        }
    }

    @Override
    protected void setup(Context context) throws IOException,
            InterruptedException {
        super.setup(context);
    }
}