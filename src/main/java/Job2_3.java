import node.NodePrefix;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.ToolRunner;
import reducer.BasicSumReducer;
import reducer.MultipleOutputReducer2_3;
import writable.BasicWritable;

import java.io.IOException;

public class Job2_3 {

    private final static Class thisClass = Job2_3.class;
    public static final String N_OUT_DEGREE = "outDegree";
    public static final String N_IN_DEGREE = "inDegree";

    static int printUsage() {
        System.out.println(thisClass.getName() + " [-m <maps>] [-r <reduces>] <input> <output>");
        ToolRunner.printGenericCommandUsage(System.out);
        return -1;
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();

        JobProperties prop = new JobProperties(thisClass);

        conf.setInt("mapreduce.job.maps", prop.getMaps());
        conf.setInt("mapreduce.job.reduces", prop.getReduces());

        Path input = new Path(prop.getInputPath());
        Path output = new Path(prop.getOutputPath());

        Job job = Job.getInstance(conf);
        job.setJarByClass(thisClass);
        job.setJobName(thisClass.getName());

        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);

        job.setMapperClass(JobMapper.class);
        job.setCombinerClass(BasicSumReducer.class);
        job.setReducerClass(MultipleOutputReducer2_3.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        MultipleOutputs.addNamedOutput(job, N_OUT_DEGREE, TextOutputFormat.class, IntWritable.class, IntWritable.class);
        MultipleOutputs.addNamedOutput(job, N_IN_DEGREE, TextOutputFormat.class, IntWritable.class, IntWritable.class);

        job.waitForCompletion(true);
    }

    public static class JobMapper extends Mapper<Text, IntWritable, Text, IntWritable> {

        @Override
        protected void cleanup(Context context) throws IOException,
                InterruptedException {
            super.cleanup(context);
        }

        private Text line = new Text();

        @Override
        protected void map(Text key, IntWritable value, Context context)
                throws IOException, InterruptedException {

            String keyString = key.toString();

            if (keyString.startsWith(NodePrefix.inp)) {
                line.set(NodePrefix.inp+ value);
                context.write(line, BasicWritable.one);
            } else if (keyString.startsWith(NodePrefix.out)) {
                line.set(NodePrefix.out+ value);
                context.write(line, BasicWritable.one);
            }
        }


        @Override
        public void run(Context context) throws IOException,
                InterruptedException {
            super.run(context);
        }

        @Override
        protected void setup(Context context) throws IOException,
                InterruptedException {
            super.setup(context);
        }

    }

}
