import node.NodePrefix;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.ToolRunner;
import reducer.BasicSumReducer;
import writable.BasicWritable;


import java.io.IOException;

/**
 * Created by andre on 27/04/2017.
 */
public class Job1 {

    private final static Class thisClass = Job1.class;

    static int printUsage() {
        System.out.println(thisClass.getName() + " [-m <maps>] [-r <reduces>] <input> <output>");
        ToolRunner.printGenericCommandUsage(System.out);
        return -1;
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();

        JobProperties prop = new JobProperties(thisClass);

        conf.setInt("mapreduce.job.maps", prop.getMaps());
        conf.setInt("mapreduce.job.reduces", prop.getReduces());

        Path input = new Path(prop.getInputPath());
        Path output = new Path(prop.getOutputPath());

        Job job = Job.getInstance(conf);
        job.setJarByClass(thisClass);
        job.setJobName(thisClass.getName());

        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);

        job.setMapperClass(JobMapper.class);
        job.setCombinerClass(BasicSumReducer.class);
        job.setReducerClass(BasicSumReducer.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.waitForCompletion(true);
    }

    public static class JobMapper extends Mapper<Text, IntWritable, Text, IntWritable> {

        @Override
        protected void cleanup(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.cleanup(context);
        }


        @Override
        protected void map(Text key, IntWritable value, Context context)
                throws IOException, InterruptedException {

            String keyString = key.toString();

            Text nodeLine = new Text();
            Text edgeLine = new Text();

            nodeLine.set(NodePrefix.nod);
            edgeLine.set(NodePrefix.edg);

            if (keyString.startsWith(NodePrefix.nod)) {
                context.write(nodeLine, BasicWritable.one);
            } else if (keyString.startsWith(NodePrefix.edg)) {
                context.write(edgeLine, BasicWritable.one);
            }

        }


        @Override
        public void run(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.run(context);
        }

        @Override
        protected void setup(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.setup(context);
        }

    }

}
