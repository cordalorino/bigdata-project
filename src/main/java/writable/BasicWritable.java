package writable;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;

/**
 * Created by andre on 27/04/2017.
 */
public class BasicWritable {

    public final static IntWritable zero = new IntWritable(0);
    public final static IntWritable one = new IntWritable(1);
}
