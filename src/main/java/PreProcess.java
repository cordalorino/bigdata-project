import node.NodeHelper;
import node.NodePrefix;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.ToolRunner;
import org.semanticweb.yars.nx.Node;
import org.semanticweb.yars.nx.parser.NxParser;
import reducer.BasicSumReducer;
import writable.BasicWritable;


import java.io.IOException;
import java.util.Scanner;

/**
 * Created by andre on 27/04/2017.
 */
public class PreProcess {

    private final static Class thisClass = PreProcess.class;

    static int printUsage() {
        System.out.println(thisClass.getName() + " [-m <maps>] [-r <reduces>] <input> <output>");
        ToolRunner.printGenericCommandUsage(System.out);
        return -1;
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();

        JobProperties prop = new JobProperties(thisClass);

        conf.setInt("mapreduce.job.maps", prop.getMaps());
        conf.setInt("mapreduce.job.reduces", prop.getReduces());

        Path input = new Path(prop.getInputPath());
        Path output = new Path(prop.getOutputPath());

        Job job = Job.getInstance(conf);
        job.setJarByClass(thisClass);
        job.setJobName(thisClass.getName());

        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);

        job.setMapperClass(JobMapper.class);
        job.setCombinerClass(BasicSumReducer.class);
        job.setReducerClass(BasicSumReducer.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.waitForCompletion(true);
    }

    public static class JobMapper extends Mapper<LongWritable, Text, Text, IntWritable> {


        private Text line = new Text();

        @Override
        protected void cleanup(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.cleanup(context);
        }


        @Override
        protected void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            Scanner scanner = new Scanner(value.toString());
            scanner.useDelimiter("\n");

            NxParser nxp = new NxParser();
            nxp.parse(scanner);

            for (Node[] nx : nxp) {

                /**
                 * Job 1
                 */
                line.set(NodePrefix.nod + nx[0]);
                context.write(line, BasicWritable.one);

                line.set(NodePrefix.edg + nx[1]);
                context.write(line, BasicWritable.one);

                line.set(NodePrefix.nod + nx[2]);
                context.write(line, BasicWritable.one);
                /**
                 * Job 2 - 3
                 */

                line.set(NodePrefix.inp + nx[2]);
                context.write(line, BasicWritable.one);

                line.set(NodePrefix.out + nx[0]);
                context.write(line, BasicWritable.one);

                /**
                 * Job 5
                 */

                line.set(NodePrefix.tri);
                context.write(line, BasicWritable.one);

                // nodes with a blank node as subject
                if (NodeHelper.isBlankNode(nx[0])){
                    line.set(NodePrefix.bns);
                    context.write(line, BasicWritable.one);
                }

                // nodes with a blank node as object
                if (NodeHelper.isBlankNode(nx[2])){
                    line.set(NodePrefix.bno);
                    context.write(line, BasicWritable.one);
                }

                // nodes with empty context

                if (NodeHelper.hasEmptyContext(nx)){
                    line.set(NodePrefix.eco);
                    context.write(line, BasicWritable.one);
                }

            }
        }


        @Override
        public void run(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.run(context);
        }

        @Override
        protected void setup(Context context) throws IOException,
                InterruptedException {
            // TODO Auto-generated method stub
            super.setup(context);
        }

    }

}
