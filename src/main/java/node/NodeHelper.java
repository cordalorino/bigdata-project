package node;

import org.semanticweb.yars.nx.Node;

public class NodeHelper {

    public static boolean isBlankNode(Node node) {
        return node.toString().startsWith("_");
    }

    public static boolean hasEmptyContext(Node[] nodes) {
        return nodes.length != 4;
    }
}


