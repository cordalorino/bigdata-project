package node;

/**
 * Created by andre on 27/04/2017.
 */

public interface NodePrefix {
    // input
    public final String inp = "inp";
    // output
    public final String out = "out";
    // context
    public final String con = "con";
    // node
    public final String nod = "nod";
    // edge
    public final String edg = "edg";
    // empty context
    public final String eco = "eco";
    // blank subject
    public final String bns = "bns";
    // blank object
    public final String bno = "bno";
    // triples
    public final String tri = "tri";
}


