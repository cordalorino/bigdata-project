#!/usr/bin/env bash

mkdir -p out

rm -r out/preprocess
hadoop fs -rm -r -skipTrash /acal/preprocess
yarn jar bigdata-project.jar PreProcess
hadoop fs -get /acal/preprocess out/preprocess

rm -r out/job1
hadoop fs -rm -r -skipTrash /acal/job1
yarn jar bigdata-project.jar Job1
hadoop fs -get /acal/job1 out/job1

rm -r out/job2_3
hadoop fs -rm -r -skipTrash /acal/job23
yarn jar bigdata-project.jar Job2_3
hadoop fs -get /acal/job2_3 out/job2_3

rm -r out/job4
hadoop fs -rm -r -skipTrash /acal/job4
yarn jar bigdata-project.jar Job4
hadoop fs -get /acal/job4 out/job4

rm -r out/job5
hadoop fs -rm -r -skipTrash /acal/job5
yarn jar bigdata-project.jar Job5
hadoop fs -get /acal/job5 out/job5

rm -r out/job6
hadoop fs -rm -r -skipTrash /acal/job6
yarn jar bigdata-project.jar Job6
hadoop fs -get /acal/job6 out/job6

rm -r out/job7
hadoop fs -rm -r -skipTrash /acal/job7
yarn jar bigdata-project.jar Job7
hadoop fs -get /acal/job7 out/job7


