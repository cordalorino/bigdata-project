#!/usr/bin/env bash

wget http://km.aifb.kit.edu/projects/btc-2010/btc-2010-chunk-000.gz
wget http://km.aifb.kit.edu/projects/btc-2010/btc-2010-chunk-001.gz
wget http://km.aifb.kit.edu/projects/btc-2010/btc-2010-chunk-002.gz


gunzip btc-2010-chunk-*.gz

hadoop fs -mkdir /acal/
hadoop fs -mkdir /acal/input/
hadoop fs -put btc-2010-chunk-* /acal/input/

rm btc-2010-chunk-*